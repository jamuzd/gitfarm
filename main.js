const { app, BrowserWindow } = require('electron')
const NodeGit = require('nodegit')
const path = require('path')

function createWindow() {
    let window = new BrowserWindow({
        width: 1024,
        height: 600,
        minWidth: 800,
        minHeight: 600,
        webPreferences: {
            nodeIntegration: true
        }
    })

    window.loadFile('./app/index.html')

    window.webContents.openDevTools()
}


app.on('ready', createWindow)