const NodeGit = require('nodegit')
const path = require('path')

openRepository()

var repoNameBtn = document.querySelector("#btnGroupRepoName")
var branchNameBtn = document.querySelector("#btnGroupBranch")

function openRepository() {
    // let repoPath = "/Users/dito.raafiu/Development/tvindo_backend"
    let repoPath = "/home/dito/Development/tvindo_backend"
    let pathToRepo = path.resolve(repoPath)

    NodeGit.Repository.open(pathToRepo).then(async function(repo) {
        let repository = repoPath.split("/")
        let repoName = repository[repository.length - 1]
        repoNameBtn.innerHTML = repoName + ' <span class="caret">'
        
        let currentBranch = await repo.getCurrentBranch()
        branchNameBtn.innerHTML = currentBranch.shorthand() + ' <span class="caret">'
    }, function(err) {
        console.log(err)
    })
}
